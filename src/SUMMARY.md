# Summary

[Practical Programming](./title-page.md)
[What is Programming?](./WhatIsProgramming.md)

# Foundations

- [Variables](./Variables.md)
- [Comments](./Comments.md)
- [Types](./Types.md)
- [Functions - The Basics](./FunctionBasics.md)
  - [Working with Functions](./WorkingWithFunctions.md)
  - [Scope]()
  - [Function Types]()
- [Output](./Output.md)
- [Variables in functions](./LocalVariables.md)
- [If Statements]()
- [Types - Synonyms and Newtypes]()
- [Effects]()
- [Real Output with the Console]()
- [Records]()
- [Sum Types](./SumTypes.md)
- [Case Statements](./CaseStatements.md)
- [Typeclasses and Polymorphism]()
- [Higher Order Functions]()
- [Collections - Arrays]()
- [Collections - Tuples and Maps]()
- [Pattern Matching](./PatternMatching.md)

# Design Patterns

- [Recursive Functions]()
- [Folds]()
- [Functors]()
- [Applicatives]()
- [Monads]()
