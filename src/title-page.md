# Practical Programming

_by Vance Palacio_

<br>

Welcome to my Programming book!

I've written this book as a bit of a thought experiment but my hope is that I might tear down the
"wall of intimidation" regarding this field we affectionately call Software Engineering.

Programming isn't hard! Sure, there are some feats we attempt in programming that are quite tough,
but the act of programming in itself is not hard. Programming just requires a new way of thinking,
and adapting to this takes time and careful analysis on the part of the student.

I believe the perceived difficulty people experience when approaching a new concept is due to our
teachers moving too quick. If we want to learn something at a deep level, we need to take time and
study the examples; and the examples need be full and complete. I've attempted to do this here in my
book, and I hope you will find it to be a easily digestable in comparison to any past experiences
you may have had.

This book is aimed towards non-programmers and programmers alike. Move through the book at whatever
pace feels comfortable for you, but keep in mind it's imperative that you fully understand the
examples if you wish to make swift progress. If you try to gloss over the examples without fully
understanding them, this may make a quick end to your learning experience. If the examples are just
too hard to understand, then I have done something wrong, and you should contact me so that I may
clarify these sections of the book!

I wish you the best in your learning!

---

#### P.S

Feedback is welcome! If you have any suggestions/concerns/difficulties, feel free to contact me at
book@vanceism7.ml

Thanks again!

<br>

---

<br>

Practical Programming © 2020 by Vance Palacio is licensed under CC BY-NC-SA 4.0. To view a copy of this license, visit [http://creativecommons.org/licenses/by-nc-sa/4.0/](http://creativecommons.org/licenses/by-nc-sa/4.0/)
