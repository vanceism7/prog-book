# Chapter 3

---

**Tips** ⚠️️

- Use [try.purescript.org](https://try.purescript.org) to test out the code examples in this book.
- Whenever the example code starts with `module Main where`, make sure to clear out the code editor
  on [try.purescript.org](https://try.purescript.org) before pasting new code in. This will help to
  avoid unncessary errors

---

## Types

Erase everything in your code box and then type the following into your program:

```haskell
module Main where
import Prelude

luckyNumber = 74
name = "Scrooge McDuck"
answer = luckyNumber + name
```

Ah wait, what happened? In the above example, we told the computer to do 74 + "Scrooge McDuck". But
as I'm sure you're well aware, you can't add numbers and words. This is because they aren't the same
types of things. When we create variables, each one has a specific type. The type given to the
variable is mostly based on the value you give it. Different types of values will have different
types. Numbers will have types like `Int` (as in Integer) or `Number`, text will usually have the
type `String`. Some actions, like addition, only work when both operands are numeric types. In the
above example, the compiler helps us by informing us that the types of our variables aren't right
for addition. The compiler will be your best friend when writing programs, it makes sure that the
things you tell the computer to do make sense. For example, if you replace your definition of
`luckyNumber` with the following, the compiler will complain:

```haskell
luckyNumber :: Int
luckyNumber = "Hello Fred"
```

Oh whoa wait up now! What does this weird `luckyNumber :: Int` line mean? This is called a _Type
Declaration_, they go above the variable definition. It’s basically saying “Hey compiler, I want you
to know that `luckyNumber` should be an `Int`. You can read the symbol `::` to mean “has the type”
or “is the type”. So `luckyNumber :: Int` can be literally read as “`luckyNumber` has the type
`Int`”. Most of the time, the compiler can infer the types of your variables based on the value you
give it, but often it's helpful to inform the compiler of the types you intend to use explicitly.
The more often you use type declarations, the more clear error messages will be.

### The primitive types

There's a basic set of types you will find in just about every programming language. Here's a list
of our primitives in purescript:

---

`Int` is the type of non-decimal numbers, like 10 or 450

```haskell
integer :: Int
integer = 45
```

---

`Number` is the type of fractional/decimal numbers, like 3.14, or 45.734

```haskell
decimal :: Number
decimal = 34.5
```

---

`String` is the type of text

```haskell
greetingMessage :: String
greetingMessage = "Welcome to my glorious mansion"
```

---

`Char` is the type of single character text. (I dont think this type is all that useful) To create a
char, you use single quotes `'` instead of double `"`

```haskell
favoriteLetter :: Char
favoriteLetter = 'H'
```

---

`Boolean` is the type to represent true and false.

```haskell
areYouCool :: Boolean
areYouCool = true -- I sure think so ^_^
```

---

`Unit` is a bit different of a type. It contains only a single value. It's sort of like a `Boolean`
but instead of having a `true` and `false`, it only has a `true`. That doesn't sound very useful!
Well the point of it is that we use it in cases when we don't care what our value is. You'll learn
more about this when we begin taking a deeper look at functions.

The single value belonging to the type `Unit` is `unit`. Here's an example of a `Unit` variable:

```haskell
uselessValue :: Unit
uselessValue = unit
```

---

`Void` is a type which contains no values at all! You can't create a `Void` because there's no
values we can use to create it. Although this is starting to sound very weird (even in comparison to
`Unit`), it does have some legitimate uses.

I would show an example of a `Void` variable but since we can't create them, there's nothing to
show. (There is technically a way to create them, but this is too early to introduce those concepts)

---

There are other more complex types aside from the primitives, we'll introduce those in the up and
coming chapters

### Summary

A _Type Declaration_ informs the compiler of what type you intend your variables and functions to
have (We'll get to functions soon).

Type declarations go above the variable's definition.

Heres an example type declaration

```haskell
coolNum :: Int -- Type declaration
coolNum = 5 -- variable definition
```

You can read the `::` symbol to mean "has the type" or "is the type". So the above type declaration
would be read as "`coolNum` has the type `Int`".

The primitive types are `Int`, `Number`, `String`, `Char`, `Boolean`, `Unit`, and `Void`.

---

### Self Practice

Add type declarations to the variables below

```haskell
module Main where
import Prelude

coolNumber = 50

collegeSavings = 4000

personsName = "John Wiggly"
```

When you think you've got it, check what you put against the answer below

<details>
<summary>Answer</summary>

```haskell
module Main where
import Prelude

coolNumber :: Int
coolNumber = 50

collegeSavings :: Int
collegeSavings = 4000

personsName :: String
personsName = "John Wiggly"
```

</details>
