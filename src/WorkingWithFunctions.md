# Chapter 4.1

---

**Tips** ⚠️️

- Use [try.purescript.org](https://try.purescript.org) to test out the code examples in this book.
- Whenever the example code starts with `module Main where`, make sure to clear out the code editor
  on [try.purescript.org](https://try.purescript.org) before pasting new code in. This will help to
  avoid unncessary errors

---

## Function Declarations

Now that we've had the chance to play around with functions a bit, we'll take a closer look at how
to create them.

When we learned how to create variables, I mentioned that the syntax to do this was:

> _name = value_

The syntax for creating functions closely matches this, taking the form

> _name **parameters** = definition_

That is, first we put the name of the function, then the names of its parameters, the `=` sign, and
finally the actual calculation the function will perform.

For example:

```haskell
module Main where
import Prelude

addFive num = num + 5

-- addFive is the function name
-- num (on the left side of the "=") is the parameter
-- num + 5 is the definition
```

We can name the parameters whatever we want; they use the same naming rules as variables (because
function parameters _are_ variables).

Next, lets try writing a function that multiplies its parameter by two. Paste the following into
your code editor

```haskell
module Main where
import Prelude

multByTwo theNumber = theNumber * 2

-- multByTwo is the function name
-- theNumber (on the left side of the "=") is the parameter
-- theNumber * 2 is the definition
```

Why not try a few on your own? Try writing the following functions:

1. A function called `addTen` which adds `10` to its parameter
2. A function called `sub15` which subtracts `15` from its parameter
3. A function called `square` which multiplies the parameter with itself
4. A function called `doNothing` which simply returns the parameter without doing anything to it

<details>
<summary>Answers</summary>

```haskell
module Main where
import Prelude

addTen num = num + 10

sub15 param = param - 15

square x = x * x

doNothing num = num

-- For `doNothing`, you also could have done the following:
-- doNothing num = num * 1
-- doNothing num = num + 0
```

So, how'd you do? Sorry if #4 seemed tricky, but kudos to you if you got it right! Also, don't worry
if your parameter names don't match mine; remember, you can name them whatever you want, as long as
they make sense.

</details>

## Invoking Functions

Creating functions is great, but we won't be able to enjoy them unless we actually use them. The
syntax for using a function looks like this:

> _name parameters_

The syntax to use them is nearly the same as creating them, we just omit the `=` and the definition.
Lets compare the create vs usage syntax:

```haskell
-- Function creation
addFive num = num + 5

-- Function Usage
addFive 10
```

Why did we use `num` during the creation but `10` during the usage?

As mentioned previously, `num` is a _function parameter_, which is essentially a placeholder. The
function wants to add `5` to something, but it lets whoever uses the function pick what that number
should be. The function basically says "If you want to use me, you need to tell me what `num` is
first." So then, when we use the function, we tell it to use `10` for `num`.

However, we can't just simply throw `addFive` around willy-nilly; functions return values, and
values have to be stored somewhere. Therefore, we'll need to use a variable to hold the result of
using the `addFive` function:

```haskell
module Main where
import Prelude

-- Function Declaration
addFive num = num + 5

-- Using our function
myVar :: Int
myVar = addFive 10

-- myVar = 15
```

We can use `addFive` more than once, and we can also use different values for the `num` parameter
without any issues:

```haskell
module Main where
import Prelude

-- Function Declaration
addFive num = num + 5

-- Using our function
myVar :: Int
myVar = addFive 10
-- myVar = 15

var2 :: Int
var2 = addFive 20
-- var2 = 25

var3 :: Int
var3 = addFive 0
-- var3 = 5
```

Lets try out a few more exercises.. In the above example:

1. Add another variable called `var4` and use `addFive` to set it to `6`.
2. Add another variable called `var5` and use `addFive` to set it to `100`.

<details>
<summary>Answer</summary>

```haskell
module Main where
import Prelude

module Main where
import Prelude

-- Function Declaration
addFive num = num + 5

-- Using our function
myVar :: Int
myVar = addFive 10
-- myVar = 15

var2 :: Int
var2 = addFive 20
-- var2 = 25

var3 :: Int
var3 = addFive 0
-- var3 = 5

var4 :: Int
var4 = addFive 1

var5 :: Int
var5 = addFive 95
```

If you got those right and everything is making sense, then nice work! If you're still feeling shaky
on what's happening, you might want to go back through this chapter and the previous chapter from
again. A second run through can do wonders to help solidify knowledge.

</details>

## Summary

Functions are created using the syntax

> _name parameters = definition_

Here's an example of a function that multiplies its parameter by 3

```haskell
tripleNum num = num * 3
```

To use a function, we use the syntax:

> _name parameters_

For example:

```haskell
tripleNum 20
```

Values have to be stored in a variable, so because functions return values, we need to create a
variable to store the result of our function. With that said, here's a full example of creating and
using a function.

```haskell
module Main where
import Prelude

-- Function Declaration
tripleNum num = num * 3

-- Using the function
myVar = tripleNum 20
```

## Self Practice

**Question 1**.  
Create a function that multiplies its parameter by 4, then create 2 variables that use the function

**Question 2**.  
Using the code box below, create one variable using each of the functions

```haskell
module Main where
import Prelude

addSeven num = num + 7

sub5 param = param - 5

square x = x * x
```

**Question 3**.  
Create a function that cubes its parameter, then create 2 variables that use the function

<details>
<summary>Answers</summary>

Question 1.

```haskell
module Main where
import Prelude

quadruple num = num * 4

money = quadruple 10

numberOfOnionRings = quadruple 100 -- Lets go!
```

Question 2.

```haskell
module Main where
import Prelude

addSeven num = num + 7

sub5 param = param - 5

square x = x * x

-- Variables
num1 = addSeven 7

answer2 = sub5 30

anotherVar = square 10
```

Question 3.

```haskell
module Main where
import Prelude

cubeTheNum y = y * y

-- Variables
answer1 = cubeTheNum 3

answer2 = cubeTheNum 10
```

</details>
