# Chapter 1

---

**Tips** ⚠️️

- Use [try.purescript.org](https://try.purescript.org) to test out the code examples in this book.
- Whenever the example code starts with `module Main where`, make sure to clear out the code editor
  on [try.purescript.org](https://try.purescript.org) before pasting new code in. This will help to
  avoid unncessary errors

---

## Computers memorize things

When we set an alarm clock, we pick our inputs and the alarm remembers them and uses them; when we
program, the same principles apply. Let's try out a few examples.

Before we start, clear out your code editor and paste the following code in

```haskell
module Main where
import Prelude
```

<br/>
Now, let's say we want to instruct the computer to remember a number for us. Maybe it's our lucky
number. To do this, type the following on a blank line:

```haskell
luckyNumber = 74
```

That's it. Whenever this program runs, if you ask it what `luckyNumber` is, it will say 74. How
about our name?

```haskell
name = "Scrooge McDuck"
```

There we go, now we are Scrooge McDuck! Notice the quotes above. Names aren't numbers (usually), but
a string of text instead. Whenever we want the computer to remember some text, we put that text in
quotes.

### Variable Declarations

These pieces of data we're storing in our program are called variables. Variables have a name and a
value. The act of creating a variable is called a _Variable Declaration_. You declare them using the
general form:

_name = value_

The left side of the equals sign is the name of the variable, and the right side of the equals is
the value given to it. Like our examples, you'll want to name your variables based on what they
represent. If you want to store the name of your college for example, `collegeName` might be a good
variable name to use.

```haskell
collegeName = "Smart People University"
```

If you want to store the name of your favorite food, `favoriteFood` might be good.  
**Note:** Variable names must start with a lowercase letter (or an `_`) and spaces arent allowed.

```haskell
favoriteFood = "Super Burrito"
```

But hmm, what's the point of all of this? You could just as easily type this up in a text document!
The difference is, once the computer has some variables in memory, we can do things with them… Let's
try some math then!

### Addng Variables

Clear out the code editor and add the following lines:

```haskell
module Main where
import Prelude

johnsMoney = 150
bensMoney = 300
total = johnsMoney + bensMoney
```

We create variables using the _name = value_ syntax as stated above. This assigns the value to that
variable name. Once we've stored some variables in our program, we can get those values back by
using those variable names later in the code. Whenever the computer sees one of those variables, it
will substitute the variable with it's value. So in the line `total = johnsMoney + bensMoney`

- First, the computer sees `johnsMoney` and says "`johnsMoney` was set to 150",
- then it sees `bensMoney` and says "`bensMoney` was set to 300"
- and finally, it substitutes those values in the place of the variables, turning  
  `total = johnsMoney + bensMoney` into  
  `total = 150 + 300`

We're also creating a new variable called `total` here, and its assigned value will be the result of
150 + 300, i.e 450

Why not just write `total = 300 + 150?` Have you ever read one of those math problems that says
something like "Ben has twice as much money as john."? Using variables (and `*`, the multiplication
operator), we can express this more clearly.

```haskell
module Main where
import Prelude

johnsMoney = 200
bensMoney = johnsMoney * 2
```

Ah, now no matter what value we give `johnsMoney`, `bensMoney` will always be double. If we were to
decide to change `johnsMoney` to 400, `bensMoney` will still be double that. If we had done
something like:

```haskell
johnsMoney = 200
bensMoney = 400
```

or

```haskell
johnsMoney = 200
bensMoney = 200 * 2
```

We'd have to change `bensMoney` anytime we changed `johnsMoney` to keep accurate with the original
math problem; the relationship between John and Ben's money would be less apparent as well.

### Immutability

By the way, I wanted to take a second to note that once a variable has been assigned a value, you
can't change it later. For example, you can't do the following:

```haskell
module Main where
import Prelude

johnsMoney = 100
johnsMoney = 400
```

The inability to update a variable's value is called _Immutability_. It might seem limiting but it
helps us avoid a lot of problems down the road. But enough about that, lets move forward

### Pitfall - Using text without quotes

Before we finish up our chapter, I wanted to call your attention to an issue you may run into. When
you create a variable and you want it to hold some text, make sure that you put quotes around
that text. If not, you're likely to encounter the errors listed below

#### 1. Unknown value

Try out the following:

```haskell
module Main where
import Prelude

name = james bond
```

Using the code above, you should see an error on the right side reading:

```
Unknown value james
```

What's happening here is that the compiler thinks you're trying to use a variable called `james`. It
looks around your code for the variable declaration and when it doesn't find it, comes to
the conclusion that you tried to use a variable that doesn't exist! Change the above to
`"james bond"` and everything will work as intended.

#### 2. Unknown Data Constructor

Here's another:

```haskell
module Main where
import Prelude

name = John Smith
```

This code will give you the following error:

```
Unknown data constructor John
```

I won't venture to explain exactly what this means yet, but again, the program is looking for a
_declaration_ of something called `John` and not finding it. Changing the definition to
`"John Smith"` will fix this error.

---

### Summary

Variables store data for us.

Variables are declared using the general syntax _name = value_

Heres an example of declaring some variables

```haskell
myVariable = 100

otherVar = "hello"
```

---

### Self Practice

Create 3 variables to store your name, age, and your most hated dessert! Once you're finished,
expand the example answer below to see if what you have looks similar. Who knows, maybe we even hate
the same dessert!

<details>
<summary>Answer</summary>

```haskell
module Main where
import Prelude

name = "Vance Palacio"
age = 32
topHatedDessert = "Mint Chocolate"
```

</details>

If what you've done looks similar to the above, and you have no compile errors, then good job! It's
ok if you named your variables differently, as long as they make sense!
