# Chapter 2

---

**Tips** ⚠️️

- Use [try.purescript.org](https://try.purescript.org) to test out the code examples in this book.
- Whenever the example code starts with `module Main where`, make sure to clear out the code editor
  on [try.purescript.org](https://try.purescript.org) before pasting new code in. This will help to
  avoid unncessary errors

---

## Comments

Before we continue onward, I wanted to take a brief detour to talk about comments. Comments are code
that's completely ignored. You can use them to describe code in your program. This helps other
readers (and yourself) to more easily understand the purpose of your code. You create comments by
typing a `--`. Anything following the `--` will be ignored by the compiler.

```haskell
module Main where
import Prelude

-- This is a comment.
-- You can type anything here,
-- it won't break the program

-- In the equation of a line, m is slope
m = 2

-- Equation of a line y = mx + b
-- where x = 6, b = 12
y = (m * 6) + 12

z = 15 -- The third dimension
```

These are examples of single line comments. They only effect the line they occur on and only
everything after the `--`.

We also have multi-line comments. They start with the `{-` characters and end with `-}`. Everything
you type between them is counted as comment Here's an example:

```haskell
module Main where
import Prelude

{-
This comment spans
multiple lines.

Set x to 70, the amount of
calories in an egg
-}
x = 70
```

A simple concept but very useful, especially when you find yourself looking back on code you wrote 9
months ago, or reading code someone else wrote.
