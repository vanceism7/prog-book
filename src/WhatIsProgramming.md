# Chapter 0

## What is Programming?

To answer this question, it's probably helpful to answer another: What is a computer? Practically
speaking, a computer is a data tool. Computers store vast amounts of data (such as music, photos,
email, homework, social media profiles, etc) and allow us to retrieve it later. They also provide
safeguards to ensure we interact with our data correctly. Chances are, if you had to (and could)
manually upload a photo on instagram without the app, it would take you a lot longer than 20 seconds
to do it.

But what is Programming? Programming is feeding instructions to a computer to make it do something.
Need to tell your phone to wake you up at 5:30 AM? Or do you wish you had a calculator to do your
algebra homework for you? Do you need to launch the missiles? Any time you convince a device to
carry out a task for you, you are in effect, programming. Some tasks are more difficult than others,
but with enough diligence and creativity, it can probably be done.

So how do we harness this power? It all comes down to data. At the heart of programming, we have two
main tools at our disposal: _Input_ and _Output_. Know something the computer doesn't (like your
Gmail username)? Tell the computer what it is (input) and it'll be able to use that. Do you need
info from the computer? Maybe a new email came in and you want to read it? Ask it to retrieve that
data and it will show you (output). Computers act on input and respond with output.

Wait a second, you might say, setting an alarm clock is not really "programming!" Ah, the principal
difference between setting an alarm clock and programming is only in the choice of inputs you are
allowed. An alarm clock allows only a few specific inputs: The time it should ring, which days it
should ring, alarm volume, which alarm sound should be used, etc. When we write software, the
variations of input are much greater, and also much more primitive. We don't deal with alarm times,
or days of the week; we deal with numbers, letters, and symbols. This may sound very limiting, but
like legos, we can build some very interesting things from some very simplistic parts.

### Following Along

You can follow along on your computer at [try.purescript.org](https://try.purescript.org)
(this doesn't work on phones unfortunately).
In general, the code editor should always start with the following lines

```haskell
module Main where
import Prelude
```

- Code blocks starting with the above lines should usually be safe to type/paste into the code
  editor without any worry of errors; just make sure to clear out the code editor before pasting new
  code in.
- Code blocks **without** the above lines may or may not work when pasted into the code editor. Not
  all code examples are intended to be typed in the code editor, but I will do my best to ensure the
  intent on how to use the code samples throughout this book are clear.
- Despite the previous bullet point, I encourage you to experiment where ever your curiosity is
  piqued. But try not to get too hung up on any particular section, you may miss the forest for the
  trees.

As you follow along, notice that any time you type anything, there are various symbols appearing on
the left. If you move your mouse over these symbols, you'll see different warnings and errors from
the _compiler_. These messages are here to help and guide you.  
What's a _compiler_? It's a tool that translates your code (the text you type) into instructions
that can be run by the computer. Writing code can be tough, and sometimes we'll make mistakes;
whenever the compiler detects errors in your code, it will give you a description of the error along
with which line the error has occurred. It will also show warnings as an ⚠️ symbol. You may safely
ignore those for the time being.

### Conventions

I want to keep the main conventions on how to follow along with this book clear and easily
accessible. In order to achieve this goal, I will include the following section of tips at the
beginning of each chapter:

- Use [try.purescript.org](https://try.purescript.org) to test out the code examples in this book.
- Whenever the example code starts with `module Main where`, make sure to clear out the code editor
  on [try.purescript.org](https://try.purescript.org) before pasting new code in. This will help to
  avoid unncessary errors
