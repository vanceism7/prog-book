with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "Shell";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    mdbook
  ];
}
